Source: r-cran-tfisher
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-r,
               r-base-dev,
               r-cran-sn,
               r-cran-mvtnorm,
               r-cran-matrix
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-tfisher
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-tfisher.git
Homepage: https://cran.r-project.org/package=TFisher
Rules-Requires-Root: no

Package: r-cran-tfisher
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R optimal thresholding fisher's P-value combination method
 This GNU R package provides the cumulative distribution function (CDF),
 quantile, and statistical power calculator for a collection of
 thresholding Fisher's p-value combination methods, including Fisher's
 p-value combination method, truncated product method and, in particular,
 soft-thresholding Fisher's p-value combination method which is proven to
 be optimal in some context of signal detection. The p-value calculator
 for the omnibus version of these tests are also included. For reference,
 please see Hong Zhang and Zheyang Wu. "TFisher Tests: Optimal and
 Adaptive Thresholding for Combining p-Values", submitted.
